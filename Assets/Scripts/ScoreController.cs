﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour{

    public int currentScore { get; private set; }

    [Header("Score Highlight")]
    public int scoreHighlightRange;
    public CharacterSoundController sound;

    private int lastScoreHightlight = 0;

    void Start()    {
        //reset 
        currentScore = 0;
        lastScoreHightlight = 0; 
    } 

    public void IncreaseCurrentScore(int Value) {
        currentScore += Value;

        if(currentScore - lastScoreHightlight > scoreHighlightRange){
            sound.PlayScoreHighlight();
            lastScoreHightlight += scoreHighlightRange;
        }
    }

    public void FinishScoring() {
        // set high score
        if(currentScore > ScoreData.highScore) {
            ScoreData.highScore = currentScore;
        }
    }

}
