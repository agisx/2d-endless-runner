﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class UIGameOverController : MonoBehaviour
{
    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            // reload
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
