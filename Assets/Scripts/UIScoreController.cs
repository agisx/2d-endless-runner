﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIScoreController : MonoBehaviour
{

    [Header("UI")]
    public Text score;
    public Text highScore;

    [Header("Score")]
    public ScoreController scoreController;

    void Update()
    {
        score.text = scoreController.currentScore.ToString();
        highScore.text = ScoreData.highScore.ToString();

    }
}
