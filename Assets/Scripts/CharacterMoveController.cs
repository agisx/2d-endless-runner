﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoveController : MonoBehaviour{

    [Header("Movement")] 
    public float moveAcceleration;
    public float maxSpeed;

    [Header("Jump")]
    public float jumpAcceleration;
    private bool isJumping;
    private bool isOnGround;

    [Header("Ground Raycast")]
    public float groundRaycastDistance;
    public LayerMask groundLayerMask;

    private Rigidbody2D rgb2d; 
    private Animator anim;
    private CharacterSoundController sound;
     
    [Header("Scoring")]
    public ScoreController score;
    public float scoringRatio;
    private float lastPositionX;

    [Header("Game Over")]
    public GameObject gameOverScreen;
    public float fallPositionY;

    [Header("Camera")]
    public CameraMoveController gameCamera;

    // Start is called before the first frame update
    void Start(){
        rgb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sound = GetComponent<CharacterSoundController>();
    }

    // Update is called once per frame
    void Update(){
        //read input
        if (Input.GetMouseButton(0)) {
            if (isOnGround) { 
                isJumping = true;
                sound.PlayJump();
            }
        }
        // change animation
        anim.SetBool("isOnGround", isOnGround);

        //calculate score
        int distancePassed = Mathf.FloorToInt(transform.position.x - lastPositionX);
        int scoreIncrement = Mathf.FloorToInt(distancePassed / scoringRatio);

        if(scoreIncrement > 0) {
            score.IncreaseCurrentScore(scoreIncrement);
            lastPositionX += distancePassed;
        }
        //game over
        if (transform.position.y <= fallPositionY) {
            GameOver();
        }

    }

    private void GameOver() {
        //set high score
        score.FinishScoring();

        //stop camera movement
        gameCamera.enabled = false;

        //show game over
        gameOverScreen.SetActive(true);

        //disable this too
        this.enabled = false;
    }

    private void FixedUpdate() {
        Vector2 velocityVector = rgb2d.velocity;
        //raycast ground
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, groundRaycastDistance, groundLayerMask);
        if (hit) {
            if(!isOnGround && rgb2d.velocity.y <= 0) {
                isOnGround = true;
            }
        } else {
            isOnGround = false;
        }

        //calculate velocity vector 
        if (isJumping) {
            velocityVector.y += jumpAcceleration;
            isJumping = false;
        }

        velocityVector.x = Mathf.Clamp(velocityVector.x + moveAcceleration * Time.deltaTime, 0.0f, maxSpeed);

        rgb2d.velocity = velocityVector;
    }

    private void OnDrawGizmos() {
        Debug.DrawLine(transform.position, transform.position + (Vector3.down * groundRaycastDistance), Color.white);
    }
}
